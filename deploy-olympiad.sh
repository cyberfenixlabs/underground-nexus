#!/bin/sh

set -x

dockerd &

service=Docker

if [[ $(ps -ef | grep -v grep | grep $service | wc -l) > 0 ]]; then
    echo "$service is running"
else
    echo "Oops Docker seemed to be running already"
    rm /var/run/docker.pid
fi

sleep 30

docker swarm init

# Create Docker network `Inner-Athena`
docker network create --driver bridge \
    --subnet=10.20.0.0/24 Inner-Athena

# Run Pi-hole DNS docker node
docker run -itd \
    -p 53:53/tcp \
    -p 53:53/udp \
    -p 67:67 \
    -p 80:80 \
    -p 443:443 \
    --name=Inner-DNS-Control \
    --hostname=Inner-DNS-Control \
    --net=Inner-Athena \
    --ip=10.20.0.20 \
    --restart=always \
    -v pihole_DNS_data:/etc/dnsmasq.d/ -v /var/lib/docker/volumes/pihole_DNS_data/_data/pihole/:/etc/pihole/ pihole/pihole:latest

# Create volume `portainer_data` for Olympiad0 Portainer docker node
docker volume create portainer_data

# Run Olympiad0 Portainer docker node
docker run -d \
    -p 8000:8000 \
    -p 9443:9443 \
    --name=Olympiad0 \
    --dns=10.20.0.20 \
    --net=Inner-Athena \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data cr.portainer.io/portainer/portainer-ce

# Run HashiCorp Vault in development mode
# @TODO build out a prod version for security reason
docker run -itd \
    -p 8200:1234 \
    --name=Nexus-Secret-Vault \
    -h Nexus-Secret-Vault \
    --dns=10.20.0.20 \
    --net=Inner-Athena \
    --restart=always \
    --cap-add=IPC_LOCK \
    -e 'VAULT_DEV_ROOT_TOKEN_ID=myroot' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:1234' vault

# Run Workbench in order for the docker node to run so that the entry script from Kali Linux is executed correctly
docker run -itd \
    -p 1000:3000 \
    -e PUID=1000 -e PGID=1000 -e TZ=America/Colorado \
    --name=Workbench \
    --hostname=Workbench \
    --privileged \
    --init \
    --dns=10.20.0.20 \
    --net=Inner-Athena \
    --restart=always \
    -v workbench0:/config -v /nexus-bucket:/config/Desktop/nexus-bucket -v /var/run/docker.sock:/var/run/docker.sock linuxserver/webtop:ubuntu-mate

# Run Security-Operation-Center for the same reason as above
docker run -itd \
    -p 2000:3000 \
    -e PUID=2000 -e PGID=2000 -e TZ=America/Colorado \
    --name=Security-Operation-Center \
    --hostname=Security-Operation-Center \
    --privileged \
    --init \
    --dns=10.20.0.20 \
    --net=Inner-Athena \
    --ip=10.20.0.30 \
    --restart=always \
    -v security-operation-center:/config -v /nexus-bucket:/config/Desktop/nexus-bucket linuxserver/webtop:alpine-kde

# Run Cyber Torpedo Life
docker run -itd \
    --privileged \
    -p 9000:9000 \
    -p 9010:9001 \
    --name=torpedo \
    -h torpedo \
    --dns=10.20.0.20 \
    --net=Inner-Athena \
    --restart=always \
    -v /nexus-bucket:/nexus-bucket -v /nexus-bucket/s3-torpedo:/data quay.io/minio/minio server /data --console-address ":9001"

# Run Athena0
docker run -itd \
    -p 22:22 \
    --name=Athena0 \
    --hostname=Athena0 \
    --dns=10.20.0.20 \
    --net=Inner-Athena \
    --restart=always \
    -v athena0:/home/ \
    -v /nexus-bucket:/nexus-bucket \
    -v /etc/docker:/etc/docker \
    -v /usr/local/bin/docker:/usr/local/bin/docker \
    -v /var/run/docker.sock:/var/run/docker.sock kalilinux/kali-bleeding-edge

docker exec Athena0 /bin/bash -c 'apt-get update -y -qq && apt-get upgrade -y -qq'

docker exec Athena0 /bin/bash -c 'DEBIAN_FRONTEND=noninteractive apt-get install -qq -y --no-install-recommends wget nano nmap wireshark git build-essential metasploit-framework unzip'

docker exec Athena0 /bin/bash -c 'git clone https://github.com/radareorg/radare2 && sh radare2/sys/install.sh'

docker exec Athena0 /bin/bash -c 'wget -O terraform-amd64.zip https://releases.hashicorp.com/terraform/1.1.7/terraform_1.1.7_linux_amd64.zip && unzip terraform-amd64.zip && mv terraform usr/local/bin'

docker exec Athena0 /bin/bash -c 'touch /root/.bashrc && terraform -install-autocomplete'

# docker exec Athena0 /bin/bash -c 'docker build https://gitlab.com/cyberfenixlabs/nexus-bucket.git#main:/'

# Optional
# Deploy KuberNexus with `k3d`
