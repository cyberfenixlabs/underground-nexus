FROM docker:dind

EXPOSE 22 53 80 443 1000 2375 2376 2377 9010 9443 18443

VOLUME ["/var/run", "/var/lib/docker/volumes", "/nexus-bucket"]

RUN apk update && apk upgrade

RUN apk add bash nano curl wget docker-compose

COPY deploy-olympiad.sh /deploy-olympiad.sh

RUN chmod +x /deploy-olympiad.sh

ENTRYPOINT [ "./deploy-olympiad.sh" ]

CMD "./deploy-olympiad.sh"